class TodosController < ApplicationController
  before_action :todo_find, only: [:show, :edit, :update, :destroy, :complete]

  def index
    @todos = Todo.all
  end

  def list
    todos = Todo.all
    @completed = todos.where(completed: true)
    @not_completed = todos.where(completed: false)
  end

  def new
    @todo = Todo.new
  end

  def create
    @todo = Todo.new(todo_params)
    @todo.save
    redirect_to root_path
  end

  def show
    #@todo = Todo.find(params[:id])
  end

  def edit
    #@todo = Todo.find(params[:id])
  end

  def update
    #@todo = Todo.find(params[:id])
    @todo.update(todo_params)
    redirect_to root_path
  end

  def destroy
    #@todo = Todo.find(params[:id])
    @todo.destroy
    redirect_to root_path
  end

  def complete
    #@todo = Todo.find(params[:id])
    @todo.completed = true
    @todo.save
    redirect_to root_path
  end

  private
  def todo_params
    params.require(:todo).permit(:description, :completed)
  end

  def todo_find
    @todo = Todo.find(params[:id])
  end
end
